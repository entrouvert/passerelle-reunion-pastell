# -*- coding: utf-8 -*-

import json

from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
import django_webtest
import httmock
import pytest

from passerelle_reunion_pastell.models import PastellReunionConnector
from passerelle.base.models import ApiUser, AccessRight


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    cache.clear()
    return django_webtest.DjangoTestApp()


@pytest.fixture
def connector(db):
    # création du connecteur et ouverture de la permission "can_access" sans authentification.
    connector = PastellReunionConnector.objects.create(slug='test', url='https://some-url')
    api = ApiUser.objects.create(username='all', keytype='', key='')
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
            codename='can_access', apiuser=api,
            resource_type=obj_type, resource_pk=connector.pk)


def test_create_document(app, connector):
    import requests_mock
    with requests_mock.Mocker() as m:
        m.post('https://some-url', status_code=200)
        resp = app.post('/passerelle-reunion-pastell/test/createDocument')
    assert resp.json['data']['response'] == 200
