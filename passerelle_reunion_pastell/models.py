# -*- coding: utf-8 -*-
import json

from django.db import models
from django.utils.translation import ugettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
# from passerelle.utils.jsonresponse import APIError



class PastellReunionConnector(BaseResource):

    url = models.URLField(max_length=128, verbose_name=_('Pastell API URL'))
    # id_e = models.IntegerField(verbose_name=_('Pastell id entité'))
    type_flux = models.CharField(max_length=128, verbose_name=_('Pastell type de flux'), default='Type_flux')

    category = 'Divers'

    class Meta:
        verbose_name = u'Connecteur pastell réunion'

    @endpoint(description_get=_('CreateDocument'), methods=['post'], perm='can_access')
    def createDocument(self, request):
        # try:
        #     data = json.loads(request.body)
        # except ValueError as e:
        #     return True, "could not decode body to json: %s" % e, None
        
        # url = self.url + '/api/v2/entite/' + self.id_e + '/document'

        res = self.requests.post(self.url, auth = ('admin', 'dF7#A9pn?'), data = {'type' : self.type_flux}, verify=False)
        return {'data': {'response': res.status_code}}
