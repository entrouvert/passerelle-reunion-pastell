# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-10-15 11:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_reunion_pastell', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pastellreunionconnector',
            name='type_flux',
            field=models.CharField(default=b'Type_flux', max_length=128, verbose_name='Pastell type de flux'),
        ),
    ]
